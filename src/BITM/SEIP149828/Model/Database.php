<?php
namespace App\Model;
use PDO;
use PDOException;

class Database{
    public $DBH;

    public $username='root';
    public $pass="";

    public function __construct()
    {
        try{
            $DBH= new PDO("mysql:host=localhost;dbname=atomic_project_b37", $this->username, $this->pass);
            echo"success";
        }catch (PDOException $e){
            echo $e->getMessage();
        }
    }
}

//$obj= new Database();
